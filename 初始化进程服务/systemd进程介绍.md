> 在systemd之前，linux下初始化进程服务是`system v init`（具体用法就不了解了，直接学新的就好）

> 使用了systemd初始化进程后最直观的就是linux的开机速度得到了不小的提升（采用了并发启动机制，原来的init是串行滴-只有前一个进程启动完，才会启动下一个进程）

## 1. systemd的作用
书接上回：当加载linux内核后，开始运行systemd进程进行用户/系统进程的初始化，而systemd也不是一个命令，而是一组命令的集合，其广泛用于管理系统资源（确实很广，也很复杂）。我们常见的`systemctl`是systemd的主命令。

## 2. systemd常见的系统管理命令：
- systemctl：用于管理系统
- systemd-analyze：查看启动耗时
- hostnamectl：当前主机信息设置
- locatlctl：本地化信息设置
- timedatectl：当前时区信息设置
- loginctl：当前登录用户信息设置

## 3. 系统资源Unit（systemd可以管理的系统资源如下）：
- Service：系统服务（如果不指明资源后缀，会默认解析该资源为`xxx.service`）
- Target：多个资源组成的一个组
- Device：硬件设备
- Mount：文件挂载
- Automount：自动挂载相关
- Path：文件/路径
- Slice：进程组
- Scope：外部进程
- Snapshot：systemd快照
- Socket：进程通信相关
- Swap：缓存文件
- Time：定时器
- （这都是啥，就用过上面列出的其中五种系统资源）

## 4. 查看资源状态
- `systemctl status 某个unit名称`
- `systemctl is-active 某个unit名称` # 查询某个资源是否在运行
- `systemctl is-failed 某个unit名称` # 判断某个资源是否处于启动失败状态
- `systemctl is-enabled 某unit名称`  # 判断某个资源是否建立了开机启动链接

## 5. 资源相关操作（主要是操作service服务）
- `systemctl cat xxx`    # 查看资源的配置内容
- `sudo systemctl start xxx`  # 立即启动某资源（服务）
- `sudo systemctl stop xxx`   # 立即停止某资源（服务）
- `sudo systemctl restart xx` # 重启某资源（服务）
- `sudo systemctl kill xxx`   # 杀死某个资源（服务）的所有子进程
- `sudo systemctl reload xxx` # 重新加载一个资源的配置（修改资源配置文件后重载生效）
- `sudo systemctl daemon-reload` # 重载所有修改过的资源（放便快捷）
- `systemctl show xxx`   # 显示xxx资源的底层参数（运行的进程id、运行权重等等）
- `systemctl show -p 参数 xxx` # 显示资源的指定底层参数值
- `sudo systemctl set-property xxx 参数=参数值` # 设置资源对应参数的参数值
- `systemctl list-dependencies [--all] xxxa` # 列出资源a的所有依赖资源b（如果a资源依赖b，在启动a资源前会先加载b资源）

## 6. 编写资源的配置文件
> 每个资源都对应一个配置文件，systemd会根据该文件来启动该资源

### 6.1 资源配置文件格式（大小写敏感）
- 格式：
```
[区块]
字段名=字段值  # 等号左右不能有空格哟
```

> 区块：表示资源的类型

### 6.2 Unit
> 通常是配置文件的第一个区块，用于定义该unit资源的元数据以及配置与其它资源的依赖

- Description：资源的简短描述
- Requires/Wants/BindsTo：配置当前资源的其它依赖资源
- Before/After：配置以来资源的启动顺序
- Conflicts/Condition/Assert：配置当前资源的运行条件

### 6.3 Install
> 通常是配置文件的最后一个区块，定义了该资源是怎么启动滴

- WantedBy：target组名。（比如：multi-user.target，该组下的所有资源都被设置为了开机启动，该资源配置被enable后会添加到`/etc/systemd/system/multi-user.target.wants`目录下）
- RequiredBy：target组名。资源会加入target名+.required目录下
- Alias：当前资源用于启动的别名 
- Also：当前资源激活时同时启动其指定的资源

### 6.4 Service
> 用于服务相关的配置，只有`xxx.service`资源才有该区块

- ExecStart：启动当前服务时执行的命令
- ExecStartPre：启动服务前先执行相关命令
- ExecStartPost：启动服务后执行相关命令
- ExecStop：停止当前服务时执行的命令
- Environment：指定相关环境变量
- ExecReload：重启当前服务时指定的命令
- Type：定义启动时的进程行为：
    - simple：启动主进程执行`ExecStart`中的命令
    - forking：资源将以fork()方式启动，此时父进程将会退出，子进程将成为主进程
    - oneshot/dbus/notify/idle：（没有相关使用的概念，写了也白写，到需要时自查）
- SuccessExitStatus：服务进程正常退出返回的信号量（比如java程序的143,https://serverfault.com/questions/695849/services-remain-in-failed-state-after-stopped-with-systemctl/695863#695863）
- 更多详细：https://www.freedesktop.org/software/systemd/man/systemd.unit.html

### 6.5 Target
> 多个资源组成的unit组，当启动某个target时就相当于启动了该target组下的所有资源

- `systemctl list-unit-files --type=target`：查看当前系统中的所有资源组
- `systemctl list-dependencies multi-user.target` ：查看multi-user资源组中包含的所有资源
- 常见的资源组：
    - `graphical.target`：图像界面
    - `multi-user.target`：多用户（service开机启动常用）

### 7. 源文件加入systemd
- 资源文件的存放目录：`/etc/systemd/system/`，会使用软链接[window下的快捷方式]加载`/usr/lib/systemd/system/`目录下的所有配置文件
- `sudo systemctl enable 路径/xxx`  # 提交资源给systemd管理（如果配置了开机启动，该命令会激活该资源的开机启动操作）
- `enable`相当给制定的资源配置文件（路径/xxx）创建了一个软链接到`/etc/systemd/system`目录中
- `sudo systemctl disable xxx` # 撤销链接关系

## 8. 日志管理
> systemd还统一管理了系统相关的日志，只需要`journalctl`一个命令就可以查看所有日志。

```bash
# 查看所有日志（默认情况下 ，只保存本次启动的日志）
$ sudo journalctl

# 查看内核日志（不显示应用日志）
$ sudo journalctl -k

# 查看系统本次启动的日志
$ sudo journalctl -b
$ sudo journalctl -b -0

# 查看上一次启动的日志（需更改设置）
$ sudo journalctl -b -1

# 查看指定时间的日志
$ sudo journalctl --since="2012-10-30 18:17:16"
$ sudo journalctl --since "20 min ago"
$ sudo journalctl --since yesterday
$ sudo journalctl --since "2015-01-10" --until "2015-01-11 03:00"
$ sudo journalctl --since 09:00 --until "1 hour ago"

# 显示尾部的最新10行日志
$ sudo journalctl -n

# 显示尾部指定行数的日志
$ sudo journalctl -n 20

# 实时滚动显示最新日志
$ sudo journalctl -f

# 查看指定服务的日志
$ sudo journalctl /usr/lib/systemd/systemd

# 查看指定进程的日志
$ sudo journalctl _PID=1

# 查看某个路径的脚本的日志
$ sudo journalctl /usr/bin/bash

# 查看指定用户的日志
$ sudo journalctl _UID=33 --since today

# 查看某个 Unit 的日志
$ sudo journalctl -u nginx.service
$ sudo journalctl -u nginx.service --since today

# 实时滚动显示某个 Unit 的最新日志
$ sudo journalctl -u nginx.service -f

# 合并显示多个 Unit 的日志
$ journalctl -u nginx.service -u php-fpm.service --since today

# 查看指定优先级（及其以上级别）的日志，共有8级
# 0: emerg
# 1: alert
# 2: crit
# 3: err
# 4: warning
# 5: notice
# 6: info
# 7: debug
$ sudo journalctl -p err -b

# 日志默认分页输出，--no-pager 改为正常的标准输出
$ sudo journalctl --no-pager

# 以 JSON 格式（单行）输出
$ sudo journalctl -b -u nginx.service -o json

# 以 JSON 格式（多行）输出，可读性更好
$ sudo journalctl -b -u nginx.serviceqq
 -o json-pretty

# 显示日志占据的硬盘空间
$ sudo journalctl --disk-usage

# 指定日志文件占据的最大空间
$ sudo journalctl --vacuum-size=1G

# 指定日志文件保存多久
$ sudo journalctl --vacuum-time=1years
```

## 9. 资源配置文件demo

### 9.1 编写redis到开机启动
- 下载redis软件包/解压/配置redis.conf文件
- 编写redis资源配置文件(随便一个目录下创建redis.service文件写入下面内容)
```bash
[Unit]
Description=redis-server
After=network.target
[Service]
Type=forking
ExecStart=/usr/local/bin/redis-server /usr/local/bin/redisconfig/redis.conf
PrivateTmp=true
[Install]
WantedBy=multi-user.target
```

- 激活该资源
```bash
# 执行命令后会提升加入到multi-user.target.wants目录中
sudo systemctl enable ./redis.service
```

- 运行资源内容
`sudo systemctl start redis.service`

- 资源运行状态
![](./imgs/DeepinScreenshot_select-area_20201031163137.png)
    - Loaded行：配置文件的位置，是否设为开机启动
    - Active行：表示正在运行
    - Main PID行：主进程ID
    - Status行：由应用本身（这里是 httpd ）提供的软件当前状态
    - CGroup块：应用的所有子进程
    - 日志块：应用的日志

- 停止redis服务
`sudo systemctl stop redis.service`
如果没有响应就需要进行杀死操作
`sudo systemctl kill redis.service`

- 重启redis服务（修改了redis config配置）
`sudo systemctl restart redis.service`

### 9.2 java项目开机启动

```
[Unit]
Description=lemonjamsdk
After=syslog.target

[Service]
ExecStart=/opt/openjdk11/bin/java -jar /opt/lemonjamsdk/sdk.jar
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```

> ref:
- https://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-part-two.html
- http://www.ruanyifeng.com/blog/2016/03/systemd-tutorial-commands.html
- https://wiki.archlinux.org/index.php/systemd_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)
- https://www.freedesktop.org/software/systemd/man/systemd.service.html#