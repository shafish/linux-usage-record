## 压缩与解压
> xz压缩算法突出特点：压缩率高 

### 压缩
```shell
tar zxvf xxx.tar.gz 存放压缩包目标目录
tar jcvf xxx.tar.bz2 存放压缩包目标目录
tar Jcvf xxx.tar.xz 存放压缩包目标目录
```

### 解压
```shell
tar zxvf xxx.tar.gz -C 存放解压包目标目录
tar jxvf xxx.tar.bz2 -C 存放解压包目标目录
tar Jxvf xxx.tar.xz -C 存放解压包目标目录
```