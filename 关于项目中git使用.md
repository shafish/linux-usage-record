## 1. 本地安装git
window直接到官网下载安装配置到环境变量即可：https://git-scm.com/
linux/mac自带有git

## 2. 配置git信息
### 2.1 用户名和邮箱
配置提交代码时的显示的用户名+邮箱：

```bash
git config --global user.name "昵称"
git config --global user.email "邮箱地址"
```

### 2.2 绑定帐号ssh公钥
创建一个本地电脑的公钥跟你的gitee帐号关联起来，提交代码就不用输入用户名+密码了：
- 本地生成公钥
```bash
ssh-keygen -t rsa -C "你的邮箱"  
```
执行上面命令后，一直按回车，最后会在用户目录下的.ssh目录中生成一对密钥+公钥，只需要将公钥文件中的信息复制到gitee帐号中就可以了。

打开`c/user/用户名/.ssh/id_rsa.pub`文件，把里面的内容全部复制到`https://gitee.com/profile/sshkeys`中的公钥输入框中，标题随便写:

![](https://cdn.shafish.cn/imgs/Docsify/linuxdemo/DeepinScreenshot_google-chrome_20201024221246.png)

ok!基本的git配置完成

## 3. 上传一个文件到仓库
### 3.1 在gitee中创建一个仓库
![](https://cdn.shafish.cn/imgs/Docsify/linuxdemo/DeepinScreenshot_google-chrome_20201024221538.png)

![](https://cdn.shafish.cn/imgs/Docsify/linuxdemo/DeepinScreenshot_google-chrome_20201024221911.png)

### 3.2 下载项目到本地
```bash
git clone xxx（项目地址）
cd 项目
```

### 3.3 提交一个文件到gitee创建的仓库
window直接拖一个文件到项目目录
命令行执行：
```
git add .
git commit -m "这里写提交的说明"
git push
```

ok!!去gitee看看创建的项目已经存在上面拖进来的文件咧

## 4. 给项目添加多人开发者
![](https://cdn.shafish.cn/imgs/Docsify/linuxdemo/DeepinScreenshot_google-chrome_20201024223701.png)

![](https://cdn.shafish.cn/imgs/Docsify/linuxdemo/DeepinScreenshot_google-chrome_20201024223916.png)

![](https://cdn.shafish.cn/imgs/Docsify/linuxdemo/DeepinScreenshot_google-chrome_20201024224159.png)

