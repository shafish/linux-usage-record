> 学前知识：防火墙防的是什么、是怎么处理的

- 1.防火墙基于流量的源目的地址、端口号、协议、应用等可以设置相关的策略进行监控拦截，若流量与设置的策略匹配则就进行相应的处理，如果不匹配则忽略或者说阻挡该流量的访问。
- 2.现在常见的两种软件防火墙服务为`iptables`和`firewalld`，都是用来定义防火墙策略的管理工具，真正执行策略的是linux内核中的过滤框架`netfilter`和`nftables`。
- 3.策略规则处理的流量一般分为一下几种：
    - 流入流量-`input`
    - 流出流量-`output`
    - 转发流量
    - 路由选择前的流量（路由器使用）
    - 路由选择后的流量（路由器使用）
- 4.策略规则的处理操作分为以下几种：
    - 放行-`accept`
    - 阻止-`reject`
    - 登记-`log`
    - 忽略-`drop`

?> 当`默认策略`为阻止时 只有匹配策略规则的流量才被放行，不匹配的流量都会被阻挡掉。

- 5. 防火墙策略规则的匹配是从上到下，所以优先级高的规则需要放在前面

## iptables操作基本命令
- 常用的参数
    - `-P`：设置默认策略
    - `-F`：情空规则链
    - `-L`：查看规则链
    - `-A`：在规则链末尾添加新的规则
    - `-I num`：在指定的规则链头部添加新规则(INPUT输入流量、OUTPUT输出流量)
    - `-D num`：删除某一条规则
    - `-j`：指定流量的操作处理(ACCEPT接受、REJECT拒绝)
    - `-s`：匹配流量ip/mask
    - `-d`：匹配流量目标地址
    - `-i 网卡名称`：指定匹配从该网卡input的流量
    - `-o 网卡名称`：指定匹配从该网卡output的流量
    - `-p`：流量根据协议匹配（tcp、udp等）
    - `--dport num`：匹配流量的目标端口号(num表示需要匹配的端口号)
    - `--sport num`：匹配流量的来源端口号

### 基本使用命令

- 1.查看已有的防火墙规则
```shell
iptables -L
```

- 2.设置默认策略为拒绝
```shell
iptables -P INPUT DROP
```

- 3.允许服务器ping操作
> 允许icmp协议的流量通过

```shell
# 在策略规则的开头（-I）添加一条流入（INPUT）规则：允许（-j ACCEPT）icmp协议（-p）的流量通过
iptables -I INPUT -p icmp -j ACCEPT
```

- 4.删除刚刚设置的那条流入策略规则
```shell
iptables -D INPUT 1
```

- 5.设置只允许来自特定ip的流量访问本地的22端口（ssh），其他主机的流量统统拒绝（添加访问控制，cdn对特定网站开启访问的场景可以使用）
```shell
# -s匹配流量的来源ip，--dport匹配访问本地22的流量 /24指定了该特定ip为0～24都可以访问
iptables -I INPUT -s 192.168.17.0/24 -p tcp --dport 22 -j ACCEPT
# 策略规则是从头执行到末尾，所以拒绝访问的规则放在规则链最后（-A）
iptables -A INPUT -p tcp --dport 22 -j REJECT
```
!> 特别说明：允许策略一定需要设置在拒绝策略之前

- 6.在INPUT规则链中添加拒绝所有人访问本机12345端口的策略规则
```shell
iptables -I INPUT -p tcp --dport 12345 -j REJECT
iptables -I INPUT -p udp --dport 12345 -j REJECT
```

- 7.在INPUT规则链中添加拒绝192.168.10.5主机访问本机80端口的策略规则
```shell
iptables -I INPUT -p tcp -s 192.168.10.5 --dport 80 -j REJECT
```

- 8.在INPUT规则链中添加拒绝所有主机访问本机1000～1024端口的策略规则
```shell
iptables -I INPUT -p tcp --dport 1000:1024 -j REJECT
iptables -I INPUT -p udp --dport 1000:1024 -j REJECT
```

!> 使用iptables命令设置的防火墙策略会在系统重启后生效，需要有就生效还需要执行保存命令:
```shell
service iptables save
```
此命令会将防火墙相关的配置策略保存在`/etc/iptalbes`中(manjaro系统)

### 总结
常使用的配置命令:
- `-I`指定流量是否是输入匹配（大部分）
- `-p`制定流量的类型（tcp访问大部分）
- `--dport`指定访问的接口(根据程序的访问端口匹配)
- `-j`符合策略的流量的操作(拒绝、接受)

还有不懂的地方，就反复敲打上面8个基本demo命令！！

## firewalld
